package main

import (
	"gitlab.com/chanraork/go-gql-server/pkg/server"
)

func main() {
	server.Run()
}